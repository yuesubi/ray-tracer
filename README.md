# Ray tracer
A ray tracer written in c++
(not finished though)

<img src="https://gitlab.com/yuesubi/ray-tracer/-/raw/main/res/sample.png" width="640">


## Run

### With raylib
* Clone the repo
* Compile raylib
* Replace the folowing files with the ones you just compiled
```bash
inc/raylib-cpp/raylib.h
inc/raylib-cpp/raymath.h
lib/libraylib.a  # On windows
lib/librayliblinux.a  # On linux, your libraylib.a file needs to be renamed to librayliblinux.a
```
* Make sure you have `g++`
* Type `make` in the terminal


## Architecture

### Global
```txt
+-----------------------------------+
|             Renderer              |
+---------+--------+----------------+
|         |        |     Scene      |
| Surface | Camera +--------+-------+
|         |        | Object | Light |
+---------+--------+--------+-------+
```

### Surface
```txt
+-----------------+
| Abstact Surface |
+--------+--------+
         |
     ___/_\_________________________________
    |                     |                 |
+---+----------+    +-----+-----+    +------+------+
| BasicSurface |    | RlSurface |    | SfmlSurface |
+--------------+    +-----------+    +-------------+
```

### Object
```txt
+----------------+
| Abstact Object |
+--------+-------+
         |
     ___/_\________________________
    |         |       |            |
+---+----+    |    +--+---+    +---+---+
| Sphere |    |    | Cube |    | Plane |
+--------+    |    +--+---+    +---+---+
              |        |            |
      +-------+--+     |            |
      | Triangle |<>---+------------'
      +----------+
```

### Light
```txt
+---------------+
| Abstact Light |
+--------+------+
         |
     ___/_\________________
    |                      |
+---+--------+    +--------+---------+
| PointLight |    | DirectionalLight |
+------------+    +------------------+
```


## Math

### Circle vs Ray intersection
A **Ray** is descibed by the equation :
$\vec o + \vec d t = \vec p$
with
* $\vec o$, the origin of the ray
* $\vec d$, the unit direction of the ray
* $\vec p$, a point on the ray at a distance $t$

A **Circle** is described by the equation :
$(x_{ \vec p } - x_{ \vec c })^2 + (y_{ \vec p } - y_{ \vec c })^2 + (z_{ \vec p } - z_{ \vec c })^2 = r^2$
with
* $\vec c$, the center of the circle
* $\vec r$, the radius of the circle
* $\vec p$, a point on the circle

The ray's equation can be breaked up in to three equation, one for each axis :
$$ x_{ \vec o } + x_{ \vec d } t = x_{ \vec p } $$
$$ y_{ \vec o } + y_{ \vec d } t = y_{ \vec p } $$
$$ z_{ \vec o } + z_{ \vec d } t = z_{ \vec p } $$

These equations can be inserted in the circle's equation
$$
(x_{ \vec o } + x_{ \vec d } t - x_{ \vec c })^2 +
(y_{ \vec o } + y_{ \vec d } t - y_{ \vec c })^2 +
(z_{ \vec o } + z_{ \vec d } t - z_{ \vec c })^2 = r^2
$$

We can then solve for $t$

$$
(x_{ \vec o } - x_{ \vec c } + x_{ \vec d } t)^2 +
(y_{ \vec o } - y_{ \vec c } + y_{ \vec d } t)^2 +
(z_{ \vec o } - z_{ \vec c } + z_{ \vec d } t)^2 = r^2
$$

$$
(x_{ \vec o } - x_{ \vec c } + x_{ \vec d } t)^2 +
(y_{ \vec o } - y_{ \vec c } + y_{ \vec d } t)^2 +
(z_{ \vec o } - z_{ \vec c } + z_{ \vec d } t)^2 -
r^2 = 0
$$

$$
{
    {
        2(x_{ \vec o } - x_{ \vec c })(x_{ \vec d } t) +
        (x_{ \vec o } - x_{ \vec c })^2 +
        (x_{ \vec d } t)^2
    } + {
        2(y_{ \vec o } - y_{ \vec c })(y_{ \vec d } t) +
        (y_{ \vec o } - y_{ \vec c })^2 +
        (y_{ \vec d } t)^2
    } + {
        2(z_{ \vec o } - z_{ \vec c })(z_{ \vec d } t) +
        (z_{ \vec o } - z_{ \vec c })^2 +
        (z_{ \vec d } t)^2
    } - {
        r^2
    }
} = {
    0
}
$$

$$
{
    {
        2(x_{ \vec o } - x_{ \vec c })(x_{ \vec d } t) +
        2(y_{ \vec o } - y_{ \vec c })(y_{ \vec d } t) +
        2(z_{ \vec o } - z_{ \vec c })(z_{ \vec d } t)
    } + {
        (x_{ \vec o } - x_{ \vec c })^2 +
        (y_{ \vec o } - y_{ \vec c })^2 +
        (z_{ \vec o } - z_{ \vec c })^2
    } + {
        (x_{ \vec d } t)^2 +
        (y_{ \vec d } t)^2 +
        (z_{ \vec d } t)^2
    } - {
        r^2
    }
} = {
    0
}
$$

$$
{
    {
        2 {(
            (x_{ \vec o } - x_{ \vec c }) x_{ \vec d } +
            (y_{ \vec o } -  y_{ \vec c }) y_{ \vec d } +
            (z_{ \vec o } -  z_{ \vec c }) z_{ \vec d }
        )} t
    } + { 
        { \Vert \vec o - \vec c \Vert }^2
    } + {
        (
            x_{ \vec d }^2 +
            y_{ \vec d }^2 +
            z_{ \vec d }^2
        ) t^2
    } - {
        r^2
    }
} = {
    0
}
$$

$$
2(\vec o - \vec c) \cdot \vec d t +
{ \Vert \vec o - \vec c \Vert }^2 +
{ \Vert \vec d \Vert }^2 t^2 -
r^2 = 0
$$

$$
{ \Vert \vec d \Vert }^2 t^2 +
2(\vec o - \vec c) \cdot \vec d t +
{ \Vert \vec o - \vec c \Vert }^2 - r^2
= 0
$$

Because the equation is a quadratic the solutions (if they exist) are :
$$\Delta = b^2 - 4ac$$
$$r = \frac{-b \pm \sqrt{\Delta} } { 2a }$$
with
* $a = { \Vert \vec d \Vert }^2$
* $b = 2(\vec o - \vec c) \cdot \vec d$
* $c = { \Vert \vec o - \vec c \Vert }^2 - r^2$


### 2D plane vs Ray intersection
A **Ray** is descibed by the equation :
$\vec o_r + \vec d_r t = \vec p$
with
* $\vec o_r$, the origin of the ray
* $\vec d_r$, the unit direction of the ray
* $\vec p$, a point on the ray at a distance $t$

A **2D plane** is descibed by the equation :
$(\vec p - \vec o_p) \cdot \vec n_p = 0$
with
* $\vec o_p$, a point on the 2D plane
* $\vec n_p$, the normal of the 2D plane
* $\vec p$, an other point on the 2D plane

The ray equation can be inserted in the plane's one
$$
(\vec o_r + \vec d_r t - \vec o_p) \cdot \vec n_p = 0
$$

We can then solve for $t$
$$
(\vec o_r - \vec o_p) \cdot \vec n_p +
\vec d_r t \cdot \vec n_p = 0
$$

$$
(\vec d_r \cdot \vec n_p)t = - (\vec o_r - \vec o_p) \cdot \vec n_p
$$

$$
t = - \frac {
    (\vec o_r - \vec o_p) \cdot \vec n_p
} {
    (\vec d_r \cdot \vec n_p)
}
$$

The ray will pass through the 2d plane at
$$
t = - \frac {
    (\vec o_r - \vec o_p) \cdot \vec n_p
} {
    (\vec d_r \cdot \vec n_p)
}
$$

If $\vec d_r$ is perpendicular to $\vec n_p$ then the ray and the 2D plane will never intersect, which makes sense


### Triangle vs Ray intersection
We can use the 2D plane vs ray intersection to get the point were the ray passes through the triangle. Once we are in 2D, we can use the dot product to find out if point is in the triangle.

```txt
A      H      B
 x-----x-----x
  \    : xD /
   \   :   /
    \  :  /
     \ : /
      \:/
       x
       C
```
$$
\vec H = \vec A +
\frac { \vec { AB } } { \Vert \vec {AB} \Vert } (
    \frac { \vec {AB} } { \Vert \vec {AB} \Vert }
    \cdot \vec {AC}
)
= \vec B +
\frac { \vec {BA} } { \Vert \vec {BA} \Vert } (
    \frac { \vec {BA} } { \Vert \vec {BA} \Vert }
    \cdot \vec {BC}
)
$$

$$
\vec {HD} \cdot \vec {HC} \ge 0
$$

Then repeat for the other sides