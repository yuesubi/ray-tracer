#define TRCR_USE_RAYLIB
#include "trcr/trcr.hpp"

#include "raylib-cpp/Window.hpp"


#define RENDER_RESOLUTION_WIDTH  640
#define RENDER_RESOLUTION_HEIGHT 460


trcr::Scene GenerateDemoScene()
{
	trcr::Scene scene;
	scene.backgroundColor = trcr::ColorQuantity(0.07f, 0.001f, 0.1f);

	trcr::Transform redSphereTrx(trcr::Vector3::Zero(), trcr::Quaternion::Identity());
	trcr::Sphere *redCircle = new trcr::Sphere(redSphereTrx, 0.5f, trcr::Material::Glass(),
		trcr::ColorAbsorbed(0.0f, 0.7f, 0.7f));
	scene.objects.push_back(redCircle);

	trcr::Transform blueSphereTrx(trcr::Vector3(0.5f, 0.5f, 1.0f), trcr::Quaternion::Identity());
	trcr::Sphere *blueCircle = new trcr::Sphere(blueSphereTrx, 0.5f, trcr::Material::Diffuse(),
		trcr::ColorAbsorbed::Blue());
	scene.objects.push_back(blueCircle);
	
	trcr::Transform planeTrx(trcr::Vector3(0.0f, -1.0f, 0.0f), trcr::Quaternion::Identity());
	trcr::Plane *plane = new trcr::Plane(planeTrx, trcr::Vector2 { 10.0f, 10.0f },
		trcr::Material::Metalic(), trcr::ColorAbsorbed(0.0f, 1.0f, 0.0f));
	scene.objects.push_back(plane);

	trcr::DirectionalLight *light = new trcr::DirectionalLight(trcr::Vector3(0.0f, -1.0f, 0.0f),
		trcr::ColorQuantity(1.0f, 1.0f, 1.0f));
	scene.lights.push_back(light);

	return scene;
}


void ExploreScene(trcr::Scene& scene)
{
	trcr::Transform camTrx(trcr::Vector3(0.0f, 0.0f, -1.0f), trcr::Quaternion::Identity());
	trcr::Vector2 cameraRes((float) RENDER_RESOLUTION_WIDTH, (float) RENDER_RESOLUTION_HEIGHT);
	trcr::Camera camera(camTrx, 5.0f, cameraRes);

	trcr::Renderer renderer(camera, scene);
	trcr::RaylibSurface surface(RENDER_RESOLUTION_WIDTH, RENDER_RESOLUTION_HEIGHT);

	raylib::Window win(RENDER_RESOLUTION_WIDTH, RENDER_RESOLUTION_HEIGHT, "ExploreScene");
	raylib::Texture texture = surface.GetTexture();

	SetTargetFPS(30);

	while (!win.ShouldClose()) {
		// CAMERA MOVEMENT
		trcr::Vector3 movement = trcr::Vector3::Zero();

		if (IsKeyDown(KEY_A))
			movement -= camera.transform.Right();
		if (IsKeyDown(KEY_D))
			movement += camera.transform.Right();
		if (IsKeyDown(KEY_S))
			movement -= camera.transform.Forward();
		if (IsKeyDown(KEY_W))
			movement += camera.transform.Forward();
		
		camera.transform.position += movement * 0.1f;

		// CAMERA ROTATION
		float yRotation = 0.0f;

		if (IsKeyDown(KEY_LEFT))
			yRotation -= PI / 6.0f;
		if (IsKeyDown(KEY_RIGHT))
			yRotation += PI / 6.0f;

		trcr::Quaternion torque = trcr::Quaternion::ForYAxisRotation(yRotation);

		camera.transform.rotation = torque * camera.transform.rotation;

		// RENDERERING
		BeginDrawing();
			ClearBackground(BLACK);

			renderer.RenderTo(surface);
			texture = surface.GetTexture();
			DrawTexture(texture, 0, 0, WHITE);

			DrawText("Use ZQSD/WASD to move and arrow keys to look around", 10, 10, 20, RAYWHITE);
			DrawFPS(10, 40);
		EndDrawing();
	}
}


void ExportDemoScene()
{
	trcr::Transform camTrx(trcr::Vector3(0.0f, 0.0f, -1.0f), trcr::Quaternion::Identity());
	trcr::Vector2 cameraRes((float) RENDER_RESOLUTION_WIDTH, (float) RENDER_RESOLUTION_HEIGHT);
	trcr::Camera camera(camTrx, 5.0f, cameraRes);

	trcr::Scene scene;
	scene.backgroundColor = trcr::ColorQuantity(0.07f, 0.001f, 0.1f);

	trcr::Transform redSphereTrx(trcr::Vector3::Zero(), trcr::Quaternion::Identity());
	trcr::Sphere redCircle(redSphereTrx, 0.5f, trcr::Material::Glass(),
		trcr::ColorAbsorbed(0.0f, 0.7f, 0.7f));
	scene.objects.push_back(&redCircle);

	trcr::Transform blueSphereTrx(trcr::Vector3(0.5f, 0.5f, 1.0f), trcr::Quaternion::Identity());
	trcr::Sphere blueCircle(blueSphereTrx, 0.5f, trcr::Material::Diffuse(),
		trcr::ColorAbsorbed::Blue());
	scene.objects.push_back(&blueCircle);
	
	trcr::Transform planeTrx(trcr::Vector3(0.0f, -1.0f, 0.0f), trcr::Quaternion::Identity());
	trcr::Plane plane(planeTrx, trcr::Vector2 { 10.0f, 10.0f }, trcr::Material::Metalic(),
		 trcr::ColorAbsorbed(0.0f, 1.0f, 0.0f));
	scene.objects.push_back(&plane);

	trcr::DirectionalLight light(trcr::Vector3(0.0f, -1.0f, 0.0f),
		trcr::ColorQuantity(1.0f, 1.0f, 1.0f));
	scene.lights.push_back(&light);

	trcr::Renderer renderer(camera, scene);
	trcr::RaylibSurface surface(RENDER_RESOLUTION_WIDTH, RENDER_RESOLUTION_HEIGHT);

	for (int i = 0; i < 20; i++) {
		renderer.RenderTo(surface);
		surface.GetImage().Export(TextFormat("res/progress/9/%d.png", i));
		blueCircle.transform.position += trcr::Vector3::Forward() * -0.2f;
	}
}


int main()
{
	// trcr::Scene scene = GenerateDemoScene();
	// ExploreScene(scene);

	ExportDemoScene();
}
