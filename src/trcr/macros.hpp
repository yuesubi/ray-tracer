#pragma once


#define TRCR_GETTER_SETTER(__typeOfMember, __nameOfGetterSetter, __memberName)	\
																				\
	inline __typeOfMember Get ## __nameOfGetterSetter() const					\
	{																			\
		return this->__memberName;												\
	}																			\
																				\
	inline void Set ## __nameOfGetterSetter(__typeOfMember p_newValue)			\
	{																			\
		this->__memberName = p_newValue;										\
	}
