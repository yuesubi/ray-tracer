#pragma once

#include <vector>
#include <optional>

#include "../camera/camera.hpp"
#include "../scene/scene.hpp"
#include "../surface/surface.hpp"


#define TRACE_MAX_RECURSION 10

/* The minimum distance of the ray with the object is a safety to catch objects that are behind the
 * ray but apear in front of it. This can happen for small distances because of float imprescision.
 */
#define RAY_MINIMUM_HIT_DISTANCE 0.0001f


namespace trcr {
	class Renderer
	{
	public:
		Camera& camera;
		Scene& scene;

		Renderer(Camera& camera, Scene& scene);

		void RenderTo(Surface& renderTarget) const;
	
	private:
		void DrawPixelOn(Surface& renderTarget, std::size_t x, std::size_t y) const;
		ColorQuantity Trace(const Ray& ray, const Material& currentMaterial,
			uint8_t recursiveCount = 0) const;

		Object *FindClossestHitObject(const Ray& ray) const;

		ColorQuantity FindOpaqueObjectColor(const Object *object, const HitInfo& hitInfo) const;
		ColorQuantity FindBackgroundColor(const Ray& ray) const;

		bool DoesRayHitAnObjectExcept(const Ray& ray, const Object *execptObject) const;
	};
}