#include "color_coefficients.hpp"

#include "../math/optics.hpp"


namespace trcr {
	TranslucentColorCoefficients TranslucentColorCoefficients::Workout(
		const HitInfo& hitInfo,
		const Material& currentMaterial,
		const Material& collidedMaterial)
	{
		float collideAngle = hitInfo.normal.RadianAngleWith(-hitInfo.rayDirection);

		auto sinelSolution = SinelLaw::Solve(
			collideAngle,
			currentMaterial.translucentRefrectiveIndex,
			collidedMaterial.translucentRefrectiveIndex
		);

		TranslucentColorCoefficients colorCoefs;

		if (sinelSolution.isACompleteInternalReflection) {
			colorCoefs.fromReflected = 1.0f;
			colorCoefs.fromTransmitted = 0.0f;
		} else {
			auto fresnelSolution = FresnelEquation::Solve(
				currentMaterial.translucentRefrectiveIndex,
				collidedMaterial.translucentRefrectiveIndex,
				collideAngle,
				sinelSolution.transmittedRadianAngle
			);

			colorCoefs.fromReflected = fresnelSolution.ratioReflected;
			colorCoefs.fromTransmitted = fresnelSolution.ratioTransmitted;
		}

		return colorCoefs;
	}

	ColorQuantity TranslucentColorCoefficients::Mix(
		const ColorQuantity& reflectedColor,
		const ColorQuantity& transmittedColor) const
	{
		return reflectedColor * fromReflected + transmittedColor * fromTransmitted;
	}
}