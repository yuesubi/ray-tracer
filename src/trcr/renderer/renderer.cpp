#include "renderer.hpp"

#include <math.h>

#include "color_coefficients.hpp"


namespace trcr {
	Renderer::Renderer(Camera& camera, Scene& scene)
		: camera(camera), scene(scene)
	{
	}

	void Renderer::RenderTo(Surface& renderTarget) const
	{
		camera.resolution = { (float) renderTarget.Width(), (float) renderTarget.Height() };

		for (std::size_t x = 0; x < camera.resolution.x; x++) {
			for (std::size_t y = 0; y < camera.resolution.y; y++) {
				DrawPixelOn(renderTarget, x, y);
			}
		}
	}

	void Renderer::DrawPixelOn(Surface& renderTarget, std::size_t x, std::size_t y) const
	{
		Ray pixelRay = camera.RayAtPixel({ (float) x, (float) (camera.resolution.y - y) });
		ColorQuantity outputColor = Trace(pixelRay, Material::Air());

		// TODO: detect the color range to convert color to uint8_t
		renderTarget.SetPixel(x, y, outputColor.ToUInt8(0.0f, 1.0f));
	}

	ColorQuantity Renderer::Trace(const Ray& ray, const Material& currentMaterial,
		uint8_t recursiveCount) const
	{
		if (recursiveCount >= TRACE_MAX_RECURSION)
			return ColorQuantity::None();

		Object *clossestObject = FindClossestHitObject(ray);
		if (clossestObject == nullptr) {
			return FindBackgroundColor(ray);
		}

		auto optionalHit = clossestObject->FindDetailedCollisionInfo(ray);
		if (!optionalHit.has_value()) {
			return FindBackgroundColor(ray);
		}

		HitInfo hitInfo = optionalHit.value();
		const Material& objectMaterial = clossestObject->GetMaterial();

		/* FINDING THE FINAL COLOR */

		ColorQuantity finalColor;

		if (objectMaterial.isOpaque) {
			finalColor = FindOpaqueObjectColor(clossestObject, hitInfo);
		} else {
			auto colorCoefs = TranslucentColorCoefficients::Workout(
				hitInfo,
				currentMaterial,
				objectMaterial
			);

			ColorQuantity fromReflected, fromTransmitted;

			if (colorCoefs.IsReflectedColorUsed()) {
				Ray reflectedRay = hitInfo.FindReflectedRay();
				reflectedRay.MoveOriginBy(RAY_MINIMUM_HIT_DISTANCE);
				fromReflected = Trace(reflectedRay, currentMaterial, recursiveCount + 1);
			} else {
				fromReflected = ColorQuantity::None();
			}

			std::optional<Ray> possibleRefractedRay = hitInfo.FindRefractedRay(currentMaterial,
				objectMaterial);

			if (colorCoefs.IsTransmittedColorUsed() && possibleRefractedRay.has_value()) {
				Ray refractedRay = possibleRefractedRay.value();
				refractedRay.MoveOriginBy(RAY_MINIMUM_HIT_DISTANCE);
				fromTransmitted = Trace(refractedRay, objectMaterial, recursiveCount + 1);
				// fromTransmitted.Absorb(clossestObject->GetColorAt(hitInfo.point));
			} else {
				fromTransmitted = ColorQuantity::None();
			}

			finalColor = colorCoefs.Mix(fromReflected, fromTransmitted);
		}

		// TODO: Absorb the color that commes throught the current material

		return finalColor;
	}

	Object *Renderer::FindClossestHitObject(const Ray& ray) const
	{
		Object *clossestObject = nullptr;
		float clossestHitDistance = -1.0f;

		for (Object *object : scene.objects) {
			std::optional<float> possibleHitDistance = object->FindCollisionDistance(ray);

			if (possibleHitDistance.has_value()) {
				float hitDistance = possibleHitDistance.value();

				if (clossestObject == nullptr) {
					clossestObject = object;
					clossestHitDistance = hitDistance;
				} else if (hitDistance < clossestHitDistance) {
					clossestObject = object;
					clossestHitDistance = hitDistance;
				}
			}
		}

		return clossestObject;
	}

	ColorQuantity Renderer::FindOpaqueObjectColor(const Object *object, const HitInfo& hitInfo) const
	{
		// Using Phong's model

		// CASTING SHADOW RAYS
		std::vector<Light *> lightsUsed;

		for (Light *light : scene.lights) {
			Ray shadowRay = light->GetShadowRayFrom(hitInfo.point);

			if (!DoesRayHitAnObjectExcept(shadowRay, object)) {
				lightsUsed.push_back(light);
			}
		}

		// ADD COLORS
		ColorQuantity color = ColorQuantity::None();
		const Material& material = object->GetMaterial();

		for (auto light : lightsUsed) {
			ColorQuantity lightEmited = light->ColorEmittedTo(hitInfo.point);
			
			Vector3 directionToPoint = light->LightUnitDirectionTo(hitInfo.point);
			float ratioReceived = hitInfo.normal.Dot(-directionToPoint);
			if (ratioReceived < 0.0f)
				ratioReceived = 0.0f;

			ColorQuantity lightReceived = lightEmited * ratioReceived * material.GetDiffusion();
			lightReceived.Absorb(object->GetColorAt(hitInfo.point));

			// Glossy effect
			float reflectedDotLight = hitInfo.FindReflectedRay().direction.Dot(-directionToPoint);
			float powered = std::pow(reflectedDotLight, material.opaqueShinyness);
			lightReceived += lightEmited * material.opaqueRatioReflected * powered;

			color += lightReceived;
		}

		return color;
	}

	ColorQuantity Renderer::FindBackgroundColor(const Ray& _ray) const
	{
		return scene.backgroundColor;
	}

	bool Renderer::DoesRayHitAnObjectExcept(const Ray& ray, const Object *execptObject) const
	{
		for (Object *object : scene.objects) {
			if (object != execptObject) {
				std::optional<float> hitInfo = object->FindCollisionDistance(ray);

				if (hitInfo.has_value()) {
					return true;
				}
			}
		}

		return false;
	}
}