#pragma once

#include "../camera/ray.hpp"
#include "../object/material.hpp"
#include "../surface/color.hpp"


namespace trcr {
	struct TranslucentColorCoefficients
	{
		float fromReflected;
		float fromTransmitted;

		static TranslucentColorCoefficients Workout(const HitInfo& hitInfo,
			const Material& currentMaterial, const Material& collidedMaterial);

		ColorQuantity Mix(const ColorQuantity& reflectedColor,
			const ColorQuantity& colorTransmitted) const;

		inline bool IsReflectedColorUsed() const
		{
			return fromReflected > 0.0f;
		}

		inline bool IsTransmittedColorUsed() const
		{
			return fromTransmitted > 0.0f;
		}
	};
}