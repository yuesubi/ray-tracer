#pragma once


#include "macros.hpp"

#include "camera/camera.hpp"
#include "camera/ray.hpp"

#include "light/directional_light.hpp"
#include "light/light.hpp"

#include "math/optics.hpp"
#include "math/quaternion.hpp"
#include "math/transform.hpp"
#include "math/vector2.hpp"
#include "math/vector3.hpp"

#include "object/material.hpp"
#include "object/object.hpp"
#include "object/plane.hpp"
#include "object/sphere.hpp"
#include "object/triangle.hpp"

#include "renderer/color_coefficients.hpp"
#include "renderer/renderer.hpp"

#include "scene/scene.hpp"

#include "surface/basic_surface.hpp"
#include "surface/color.hpp"
#include "surface/surface.hpp"

#ifdef TRCR_USE_RAYLIB
	#include "surface/raylib_surface.hpp"
#endif