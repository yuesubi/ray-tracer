#pragma once

#include <vector>

#include "../light/light.hpp"
#include "../object/object.hpp"
#include "../surface/color.hpp"


namespace trcr {
	struct Scene
	{
		std::vector<Light *> lights;
		std::vector<Object *> objects;

		ColorQuantity backgroundColor;
	};
}