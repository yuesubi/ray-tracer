#pragma once

#include <math.h>
#include <optional>

#include "../math/quaternion.hpp"
#include "../math/vector3.hpp"

#include "../object/material.hpp"


namespace trcr {
	struct Ray
	{
		Vector3 origin;
		Vector3 direction;

		inline Ray() = default;

		inline Ray(Vector3 origin, Vector3 direction)
			: origin(origin), direction(direction)
		{
		}

		inline void MoveOriginBy(float distance)
		{
			origin += direction * distance;
		}

		inline Vector3 PointAtDistance(float distance) const
		{
			return origin + direction * distance;
		}
	};

	struct HitInfo
	{
		float distance;
		Vector3 point;

		Vector3 rayDirection;
		Vector3 normal;

		inline HitInfo() = default;

		inline HitInfo(float distance, Vector3 point, Vector3 rayDirection, Vector3 normal)
			: distance(distance), point(point), rayDirection(rayDirection), normal(normal)
		{
		}

		inline Ray FindReflectedRay() const
		{
			Ray reflectedRay;
			reflectedRay.origin = point;
			reflectedRay.direction = (normal * 2.0f * normal.Dot(rayDirection)) - rayDirection;
			return reflectedRay;
		}

		inline std::optional<Ray> FindRefractedRay(const Material& exiting,
			const Material& entering) const
		{
			float incidentAngle = normal.RadianAngleWith(-rayDirection);

			if (incidentAngle == 0.0f)
				return Ray(point, rayDirection);

			float refrectiveRatio =
				exiting.translucentRefrectiveIndex / entering.translucentRefrectiveIndex;
			float refracteAngleSin = refrectiveRatio * std::sin(incidentAngle);

			if (refracteAngleSin > 1.0f)
				return std::nullopt;

			float refractedAngle = std::asin(refracteAngleSin);

			Vector3 rotationAxis = rayDirection.Cross(normal);
			Quaternion rotationQuat = Quaternion::ForAxisRotation(rotationAxis, refractedAngle);
			Vector3 refractedDirection = rotationQuat * (-normal);

			return Ray(point, refractedDirection);
		}
	};
}