#include "camera.hpp"


namespace trcr {
	Camera::Camera(Transform transform, float fieldOfView, Vector2 resolution)
		: transform(transform), fieldOfView(fieldOfView), resolution(resolution)
	{
	}

	Ray Camera::RayAtPixel(Vector2 pixelCoordinates) const
	{
		const float PIXEL_HALF = 0.5f;

		float xRelativePositionFromCenter =
			((float) pixelCoordinates.x + PIXEL_HALF) / (float) resolution.x
			- 0.5f;
		float yRelativePositionFromCenter =
			((float) pixelCoordinates.y + PIXEL_HALF) / (float) resolution.y
			- 0.5f;

		float heightFieldOfView = fieldOfView * ((float) resolution.y / (float) resolution.x);

		Vector3 xAxis = transform.rotation * Vector3::Right();
		Vector3 yAxis = transform.rotation * Vector3::Up();

		Vector3 positionFromCenter =
			xAxis * fieldOfView * xRelativePositionFromCenter +
			yAxis * heightFieldOfView * yRelativePositionFromCenter;

		Vector3 forward = transform.rotation * Vector3::Forward();
		Vector3 direction = (forward + positionFromCenter).Unit();

		return Ray { transform.position, direction };
	}
}