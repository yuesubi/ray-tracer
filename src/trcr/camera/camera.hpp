#pragma once

#include "../math/transform.hpp"
#include "../math/vector2.hpp"
#include "../math/vector3.hpp"
#include "ray.hpp"


namespace trcr {
	class Camera
	{
	public:
		Transform transform;
		float fieldOfView;
		Vector2 resolution;

		Camera(Transform transform, float p_FieldOfView, Vector2 resolution);

		Ray RayAtPixel(Vector2 pixelCoordinates) const;
	};
}