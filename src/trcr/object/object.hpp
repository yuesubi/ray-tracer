#pragma once

#include <optional>
#include <vector>

#include "../camera/ray.hpp"
#include "../macros.hpp"
#include "../math/transform.hpp"
#include "../surface/color.hpp"
#include "material.hpp"


namespace trcr {
	class Object
	{
	public:
		Transform transform;

		Object() = default;

		/**
		 * @brief Workouts the collision distance if the ray intersects the object. [!] This method
		 * will return the found distance, only if it is positive (>= 0.0f).
		 */
		virtual std::optional<float> FindCollisionDistance(const Ray& ray) const = 0;

		/**
		 * @brief Workouts the collision information if the ray intersects the object. [!] This
		 * method will return the hit information, only if it after the ray origin.
		 */
		virtual std::optional<HitInfo> FindDetailedCollisionInfo(const Ray& ray) const = 0;

		virtual ColorAbsorbed GetColorAt(Vector3 position) const = 0;

		TRCR_GETTER_SETTER(Material, Material, m_Material)

	protected:
		Material m_Material;
	};
}