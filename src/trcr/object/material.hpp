#pragma once

#include "../surface/color.hpp"


namespace trcr {
	struct Material
	{
		bool isOpaque;

		float opaqueRatioReflected;
		float opaqueShinyness;

		float translucentRefrectiveIndex;

		inline Material() = default;

		/**
		 * @brief Use with opaque materials only.
		 */
		inline float GetDiffusion() const
		{
			return 1.0f - opaqueRatioReflected;
		}

		static inline Material Translucent(float refrectiveIndex)
		{
			Material translucent;
			translucent.isOpaque = false;
			translucent.translucentRefrectiveIndex = refrectiveIndex;
			return translucent;
		}

		static inline Material Opaque(float ratioRelected, float shinyness)
		{
			Material opaque;
			opaque.isOpaque = true;
			opaque.opaqueRatioReflected = ratioRelected;
			opaque.opaqueShinyness = shinyness;
			return opaque;
		}

		static inline Material Air()
		{
			return Translucent(1.0f);
		}

		static inline Material Diffuse()
		{
			return Opaque(0.0f, 0.0f);
		}

		static inline Material Glass()
		{
			return Translucent(1.5f);
		}

		static inline Material Metalic()
		{
			return Opaque(0.5f, 40.0f);
		}
	};
}