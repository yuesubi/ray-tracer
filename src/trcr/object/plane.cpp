#include "plane.hpp"


namespace trcr {
	Plane::Plane(Transform transform, Vector2 dimentions, Material material, ColorAbsorbed color)
		: m_Dimentions(dimentions), m_Color(color)
	{
		this->transform = transform;
		m_Material = material;

		Vector3 topRight = transform.FromLocalToWorld(
			Vector3(dimentions.x / 2.0f, 0.0f, dimentions.y / 2.0f)
		);
		Vector3 topLeft = transform.FromLocalToWorld(
			Vector3(dimentions.x / -2.0f, 0.0f, dimentions.y / 2.0f)
		);
		Vector3 bottomRight = transform.FromLocalToWorld(
			Vector3(dimentions.x / 2.0f, 0.0f, dimentions.y / -2.0f)
		);
		Vector3 bottomLeft = transform.FromLocalToWorld(
			Vector3(dimentions.x / -2.0f, 0.0f, dimentions.y / -2.0f)
		);

		Vector3 normal = transform.rotation * Vector3::Up();

		Vector3 firstTriangleVertecies[3] = { topRight, topLeft, bottomRight };
		m_Triangles[0] = Triangle(firstTriangleVertecies, normal, material, color);
		Vector3 secondTriangleVertecies[3] = { topLeft, bottomLeft, bottomRight };
		m_Triangles[1] = Triangle(secondTriangleVertecies, normal, material, color);
	}

	std::optional<float> Plane::FindCollisionDistance(const Ray& ray) const
	{
		std::optional<float> hitDistance = m_Triangles[0].FindCollisionDistance(ray);

		if (!hitDistance.has_value())
			hitDistance = m_Triangles[1].FindCollisionDistance(ray);

		return hitDistance;
	}

	std::optional<HitInfo> Plane::FindDetailedCollisionInfo(const Ray& ray) const
	{
		std::optional<HitInfo> hitInfo = m_Triangles[0].FindDetailedCollisionInfo(ray);

		if (!hitInfo.has_value())
			hitInfo = m_Triangles[1].FindDetailedCollisionInfo(ray);

		return hitInfo;
	}

	ColorAbsorbed Plane::GetColorAt(Vector3 _position) const
	{
		return m_Color;
	}
}