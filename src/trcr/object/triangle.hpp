#pragma once

#include "object.hpp"


namespace trcr {
	class Triangle : public Object
	{
	public:
		Triangle() = default;
		Triangle(Vector3 verticies[3], Material material, ColorAbsorbed color);
		Triangle(Vector3 verticies[3], Vector3 normal, Material material, ColorAbsorbed color);

		virtual std::optional<float> FindCollisionDistance(const Ray& ray) const override;
		virtual std::optional<HitInfo> FindDetailedCollisionInfo(const Ray& ray) const override;
		virtual ColorAbsorbed GetColorAt(Vector3 position) const override;

	private:
		Vector3 m_Verticies[3];

		Vector3 m_Normal;
		Vector3 m_VerteciesProjection[3];

		ColorAbsorbed m_Color;

		bool IsProjectedPointInTriangle(Vector3 point) const;
		Vector3 ProjectionOfVertex(uint8_t vertexNumber) const;

		Vector3 NextVertex(uint8_t vertexNumber) const;
		Vector3 PreviousVertex(uint8_t vertexNumber) const;
		Vector3 ToNextVertex(uint8_t vertexNumber) const;
		Vector3 ToPreviousVertex(uint8_t vertexNumber) const;
		Vector3 NextToPrevious(uint8_t vertexNumber) const;
	};
}