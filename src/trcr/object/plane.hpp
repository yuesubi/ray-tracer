#pragma once

#include "../math/vector2.hpp"
#include "object.hpp"
#include "triangle.hpp"


namespace trcr {
	class Plane : public Object
	{
	public:
		Plane(Transform transform, Vector2 dimentions, Material material, ColorAbsorbed color);

		virtual std::optional<float> FindCollisionDistance(const Ray& ray) const override;
		virtual std::optional<HitInfo> FindDetailedCollisionInfo(const Ray& ray) const override;
		virtual ColorAbsorbed GetColorAt(Vector3 position) const override;

	private:
		Vector2 m_Dimentions;
		ColorAbsorbed m_Color;

		Triangle m_Triangles[2];
	};
}