#include "sphere.hpp"

#include <math.h>


namespace trcr {
	Sphere::Sphere(Transform transform, float radius, Material material, ColorAbsorbed color)
		: m_Radius(radius), m_Color(color)
	{
		this->transform = transform;
		m_Material = material;
	}

	std::optional<float> Sphere::FindCollisionDistance(const Ray& ray) const
	{
		Vector3 centerToRayOrigin = ray.origin - transform.position;

		float a = ray.direction.SquaredLength();
		float b = 2 * centerToRayOrigin.Dot(ray.direction);
		float c = centerToRayOrigin.SquaredLength() - m_Radius*m_Radius;

		float discriminent = b*b - 4.0f*a*c;
		if (discriminent < 0.0f)
			return std::nullopt;

		float sqrtDiscriminent = std::sqrt(discriminent);

		float hitDistance = (-b - sqrtDiscriminent) / 2.0f*a;
		if (hitDistance < 0.0f)
			hitDistance = (-b + sqrtDiscriminent) / 2.0f*a;

		if (hitDistance < 0.0f)
			return std::nullopt;

		return hitDistance;
	}

	std::optional<HitInfo> Sphere::FindDetailedCollisionInfo(const Ray& ray) const
	{
		Vector3 centerToRayOrigin = ray.origin - transform.position;

		float a = ray.direction.SquaredLength();
		float b = 2 * centerToRayOrigin.Dot(ray.direction);
		float c = centerToRayOrigin.SquaredLength() - m_Radius*m_Radius;

		float discriminent = b*b - 4.0f*a*c;
		if (discriminent < 0.0f)
			return std::nullopt;

		float sqrtDiscriminent = std::sqrt(discriminent);
		float hitDistance = (-b - sqrtDiscriminent) / 2.0f*a;

		bool usedOtherRoot = false;
		if (hitDistance < 0.0f) {
			hitDistance = (-b + sqrtDiscriminent) / 2.0f*a;
			usedOtherRoot = true;
		}

		if (hitDistance < 0.0f)
			return std::nullopt;

		HitInfo hitInfo;

		hitInfo.distance = hitDistance;
		hitInfo.point = ray.PointAtDistance(hitDistance);

		hitInfo.rayDirection = ray.direction;
		hitInfo.normal = (hitInfo.point - transform.position) / m_Radius;

		if (usedOtherRoot)
			hitInfo.normal = -hitInfo.normal;

		return hitInfo;
	}

	ColorAbsorbed Sphere::GetColorAt(Vector3 _position) const
	{
		return m_Color;
	}
}