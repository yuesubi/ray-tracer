#include "triangle.hpp"


namespace trcr {
	Triangle::Triangle(Vector3 verticies[3], Material material, ColorAbsorbed color)
		: m_Color(color)
	{
		for (uint8_t i = 0; i < 3; i++)
			m_Verticies[i] = verticies[i];

		for (uint8_t i = 0; i < 3; i++)
			m_VerteciesProjection[i] = ProjectionOfVertex(i);

		m_Normal = -ToNextVertex(0).Unit().Cross(ToPreviousVertex(0).Unit());
	}

	Triangle::Triangle(Vector3 verticies[3], Vector3 normal, Material material, ColorAbsorbed color)
		: m_Normal(normal), m_Color(color)
	{
		for (uint8_t i = 0; i < 3; i++)
			m_Verticies[i] = verticies[i];

		for (uint8_t i = 0; i < 3; i++)
			m_VerteciesProjection[i] = ProjectionOfVertex(i);
	}

	std::optional<float> Triangle::FindCollisionDistance(const Ray& ray) const
	{
		Vector3 planeToRay = ray.origin - m_Verticies[0];
		float planeToRayDotNormal = planeToRay.Dot(m_Normal);

		if (planeToRayDotNormal == 0.0f) {
			return std::nullopt;
		}

		float rayDotNormal = ray.direction.Dot(m_Normal);
		float distance = -planeToRayDotNormal / rayDotNormal;

		if (distance < 0.0f) {
			return std::nullopt;
		}

		return distance;
	}

	std::optional<HitInfo> Triangle::FindDetailedCollisionInfo(const Ray& ray) const
	{
		Vector3 planeToRay = ray.origin - m_Verticies[0];
		float planeToRayDotNormal = planeToRay.Dot(m_Normal);

		if (planeToRayDotNormal == 0.0f) {
			return std::nullopt;
		}

		float rayDotNormal = ray.direction.Dot(m_Normal);
		float distance = -planeToRayDotNormal / rayDotNormal;

		if (distance < 0.0f)
			return std::nullopt;

		Vector3 collidePoint = ray.PointAtDistance(distance);

		if (!IsProjectedPointInTriangle(collidePoint))
			return std::nullopt;

		HitInfo hitInfo;

		hitInfo.distance = distance;
		hitInfo.point = collidePoint;

		hitInfo.rayDirection = ray.direction;
		hitInfo.normal = (planeToRayDotNormal > 0.0f)? m_Normal : -m_Normal;

		return hitInfo;
	}

	Vector3 Triangle::ProjectionOfVertex(uint8_t vertexNumber) const
	{
		Vector3 unitToProjectOn = NextToPrevious(vertexNumber).Unit();
		Vector3 side = -ToNextVertex(vertexNumber);
		float bottomScale = unitToProjectOn.Dot(side);

		return NextVertex(vertexNumber) + unitToProjectOn * bottomScale;
	}

	bool Triangle::IsProjectedPointInTriangle(Vector3 point) const
	{
		for (uint8_t i = 0; i < 3; i++) {
			Vector3 projectionToPoint = point - m_VerteciesProjection[i];
			Vector3 projectionToVertex = m_Verticies[i] - m_VerteciesProjection[i];

			if (projectionToPoint.Dot(projectionToVertex) < 0.0f) {
				return false;
			}
		}

		return true;
	}

	ColorAbsorbed Triangle::GetColorAt(Vector3 position) const
	{
		return m_Color;
	}

	Vector3 Triangle::NextVertex(uint8_t vertexNumber) const
	{
		return m_Verticies[(vertexNumber + 1) % 3];
	}

	Vector3 Triangle::PreviousVertex(uint8_t vertexNumber) const
	{
		return m_Verticies[(vertexNumber + 2) % 3];
	}

	Vector3 Triangle::ToNextVertex(uint8_t vertexNumber) const
	{
		return m_Verticies[(vertexNumber + 1) % 3] - m_Verticies[vertexNumber];
	}

	Vector3 Triangle::ToPreviousVertex(uint8_t vertexNumber) const
	{
		return m_Verticies[(vertexNumber + 2) % 3] - m_Verticies[vertexNumber];
	}

	Vector3 Triangle::NextToPrevious(uint8_t vertexNumber) const
	{
		return m_Verticies[(vertexNumber + 2) % 3] - m_Verticies[(vertexNumber + 1) % 3];
	}
}