#pragma once

#include "object.hpp"


namespace trcr {
	class Sphere : public Object
	{
	public:
		Sphere(Transform transform, float radius, Material material, ColorAbsorbed color);

		virtual std::optional<float> FindCollisionDistance(const Ray& ray) const override;
		virtual std::optional<HitInfo> FindDetailedCollisionInfo(const Ray& ray) const override;
		virtual ColorAbsorbed GetColorAt(Vector3 position) const override;

	private:
		float m_Radius;
		ColorAbsorbed m_Color;
	};
}