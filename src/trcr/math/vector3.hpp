#pragma once

#include <iostream>
#include <math.h>

#include "vector2.hpp"


namespace trcr {
	/**
	 * @brief A mathematical three component vector
	 */
	class Vector3
	{
	public:
		float x, y, z;

		inline Vector3() = default;

		inline Vector3(float value)
			: x(value), y(value), z(value)
		{
		}

		inline Vector3(Vector2 xAndYCoords, float zCoord)
			: x(xAndYCoords.x), y(xAndYCoords.y), z(zCoord)
		{
		}

		inline Vector3(float xCoord, float yCoord, float zCoord)
			: x(xCoord), y(yCoord), z(zCoord)
		{
		}


		static inline Vector3 Zero()
		{
			return Vector3(0.f, 0.f, 0.f);
		}

		static inline Vector3 Forward()
		{
			return Vector3::ZAxis();
		}

		static inline Vector3 Up()
		{
			return Vector3::YAxis();
		}

		static inline Vector3 Right()
		{
			return Vector3::XAxis();
		}

		static inline Vector3 XAxis()
		{
			return Vector3(1.0f, 0.0f, 0.0f);
		}

		static inline Vector3 YAxis()
		{
			return Vector3(0.0f, 1.0f, 0.0f);
		}

		static inline Vector3 ZAxis()
		{
			return Vector3(0.0f, 0.0f, 1.0f);
		}

		inline float Length() const
		{
			return std::sqrt(x * x + y * y + z * z);
		}

		inline float SquaredLength() const
		{
			return x * x + y * y + z * z;
		}

		inline float Dot(Vector3 other) const
		{
			return x * other.x + y * other.y + z * other.z;
		}

		inline Vector3 Cross(Vector3 other) const
		{
			return Vector3 {
				y * other.z - z * other.y,
				z * other.x - x * other.z,
				x * other.y - y * other.x
			};
		}

		inline Vector3 Unit() const
		{
			return (*this) / Length();
		}

		inline float RadianAngleWith(Vector3 other) const
		{
			float dotProduct = this->Dot(other);
			float multipliedLengths = std::sqrt(this->SquaredLength() * other.SquaredLength());
			return std::acos(dotProduct / multipliedLengths);
		}

		inline bool IsZero() const
		{
			return x == 0 && y == 0 && z == 0;
		}

		inline void Print() const
		{
			std::cout << "trcr::Vector3 { x: " << x <<
				"; y: " << y <<
				"; z: " << z << "; }" << std::endl;
		}


		inline Vector3 operator+(Vector3 other) const
		{
			return Vector3 { x + other.x, y + other.y,
				z + other.z };
		}

		inline void operator+=(Vector3 other)
		{
			x += other.x;
			y += other.y;
			z += other.z;
		}

		inline Vector3 operator-(Vector3 other) const
		{
			return Vector3 { x - other.x, y - other.y,
				z - other.z };
		}

		inline Vector3 operator-() const
		{
			return Vector3 { -x, -y, -z };
		}

		inline void operator-=(Vector3 other)
		{
			x -= other.x;
			y -= other.y;
			z -= other.z;
		}

		inline Vector3 operator*(float scalar) const
		{
			return Vector3 { x * scalar, y * scalar, z * scalar };
		}

		inline void operator*=(float scalar)
		{
			x *= scalar;
			y *= scalar;
			z *= scalar;
		}

		inline Vector3 operator/(float divisor) const
		{
			return Vector3 { x / divisor, y / divisor,
				z / divisor };
		}

		inline void operator/=(float divisor)
		{
			x /= divisor;
			y /= divisor;
			z /= divisor;
		}
	};
}