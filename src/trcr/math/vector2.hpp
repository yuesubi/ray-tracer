#pragma once

#include <math.h>


namespace trcr {
	/**
	 * @brief A mathematical two component vector
	 */
	class Vector2
	{
	public:
		float x, y;

		static inline Vector2 Zero()
		{
			return Vector2 { 0.f, 0.f };
		}

		inline Vector2() = default;

		inline Vector2(float value)
			: x(value), y(value)
		{
		}

		inline Vector2(float xCoord, float yCoord)
			: x(xCoord), y(yCoord)
		{
		}

		inline float Length() const
		{
			return std::sqrt(x * x + y * y);
		}

		inline float SquaredLength() const
		{
			return x * x + y * y;
		}

		inline float Dot(Vector2 other) const
		{
			return x * other.x + y * other.y;
		}

		inline Vector2 Unit() const
		{
			return (*this) / Length();
		}

		inline Vector2 operator+(Vector2 other) const
		{
			return Vector2 { x + other.x, y + other.y };
		}

		inline void operator+=(Vector2 other)
		{
			x += other.x;
			y += other.y;
		}

		inline Vector2 operator-(Vector2 other) const
		{
			return Vector2 { x - other.x, y - other.y };
		}

		inline Vector2 operator-() const
		{
			return Vector2 { -x, -y };
		}

		inline void operator-=(Vector2 other)
		{
			x -= other.x;
			y -= other.y;
		}

		inline Vector2 operator*(float scalar) const
		{
			return Vector2 { x * scalar, y * scalar };
		}

		inline void operator*=(float scalar)
		{
			x *= scalar;
			y *= scalar;
		}

		inline Vector2 operator/(float divisor) const
		{
			return Vector2 { x / divisor, y / divisor };
		}

		inline void operator/=(float divisor)
		{
			x /= divisor;
			y /= divisor;
		}
	};
}