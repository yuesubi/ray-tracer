#pragma once

#include <math.h>


namespace trcr {
	struct SinelLaw
	{
		struct Solution
		{
			bool isACompleteInternalReflection;
			float transmittedRadianAngle;
		};

		inline static Solution Solve(float incidentRadianAngle, float exitingRefrectiveIndex,
			float enteringRefrectiveIndex)
		{
			float refrectiveRatio = exitingRefrectiveIndex / enteringRefrectiveIndex;
			float angleSin = refrectiveRatio * std::sin(incidentRadianAngle);

			Solution lawSolution;

			if (angleSin > 1.0f) {
				lawSolution.isACompleteInternalReflection = true;
				lawSolution.transmittedRadianAngle = 0.0f;
			} else {
				lawSolution.isACompleteInternalReflection = false;
				lawSolution.transmittedRadianAngle = std::asin(angleSin);
			}

			return lawSolution;
		}
	};

	struct FresnelEquation
	{
		struct Solution
		{
			float ratioReflected;
			float ratioTransmitted;
		};

		inline static Solution Solve(float exitingRefrectiveIndex, float enteringRefrectiveIndex,
			float reflectedRadianAngle, float transmittedRadianAngle)
		{
			float cosOfReflected = std::cos(reflectedRadianAngle);
			float cosOfTransmitted = std::cos(transmittedRadianAngle);

			float sReflectionSqrt =
				(exitingRefrectiveIndex * cosOfReflected -
				enteringRefrectiveIndex * cosOfTransmitted) /
				(exitingRefrectiveIndex * cosOfReflected +
				enteringRefrectiveIndex * cosOfTransmitted);

			float pReflectionSqrt =
				(exitingRefrectiveIndex * cosOfTransmitted -
				enteringRefrectiveIndex * cosOfReflected) /
				(exitingRefrectiveIndex * cosOfTransmitted +
				enteringRefrectiveIndex * cosOfReflected);

			float sReflection = sReflectionSqrt * sReflectionSqrt;
			float pReflection = pReflectionSqrt * pReflectionSqrt;

			float effectiveReflection = (sReflection + pReflection) / 2.0f;
			float effectiveTransmition = 1 - effectiveReflection;

			return Solution { effectiveReflection, effectiveTransmition };
		}
	};
}