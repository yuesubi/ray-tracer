#pragma once

#include "quaternion.hpp"
#include "vector3.hpp"


namespace trcr {
	struct Transform
	{
		Vector3 position;
		Quaternion rotation;

		inline Transform() = default;

		inline Transform(Vector3 position, Quaternion rotation)
			: position(position), rotation(rotation)
		{
		}

		inline Vector3 FromLocalToWorld(Vector3 localPosition) const
		{
			return position + (rotation * localPosition);
		}

		inline Vector3 FromWorldToLocal(Vector3 worldPostion) const
		{
			return rotation.InverseFromUnit() * (worldPostion - position);
		}

		inline Vector3 Forward() const
		{
			return rotation * Vector3::Forward();
		}

		inline Vector3 Right() const
		{
			return rotation * Vector3::Right();
		}

		inline Vector3 Up() const
		{
			return rotation * Vector3::Up();
		}
	};
}