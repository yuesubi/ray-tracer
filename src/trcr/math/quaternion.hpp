#pragma once

#include <iostream>
#include <math.h>

#include "vector3.hpp"


namespace trcr {
	class Quaternion
	{
	public:
		float w, x, y, z;

		inline Quaternion() = default;

		inline Quaternion(float w, float x, float y, float z)
			: w(w), x(x), y(y), z(z)
		{
		}

		// TODO: make a FindEulerAngles function

		/**
		 * @brief The quaternions rotates by the z, x and y axis in this
		 * order. 
		 */
		static inline Quaternion FromEulerAngles(Vector3 eulerAngles)
		{
			Quaternion xRotation = Quaternion::ForXAxisRotation(eulerAngles.x);			
			Quaternion yRotation = Quaternion::ForYAxisRotation(eulerAngles.y);
			Quaternion zRotation = Quaternion::ForZAxisRotation(eulerAngles.z);

			return yRotation * xRotation * zRotation;
		}

		static inline Quaternion FromScalarAndVector(float scalarPart, Vector3 vectorPart)
		{
			return Quaternion(scalarPart, vectorPart.x, vectorPart.y, vectorPart.z);
		}

		static inline Quaternion ForAxisRotation(Vector3 rotationAxis, float rotationInRadians)
		{
			Quaternion result;

			if (!rotationAxis.IsZero()) {
				result = ForUnitAxisRotation(rotationAxis.Unit(), rotationInRadians);
			} else {
				result = Quaternion::Identity();
			}

			return result; 
		}

		static inline Quaternion ForUnitAxisRotation(Vector3 unitRotationAxis,
			float rotationInRadians)
		{
			Quaternion result;

			float halfRotation = rotationInRadians / 2.0f;
			result = Quaternion::FromScalarAndVector(std::cos(halfRotation),
				unitRotationAxis * std::sin(halfRotation));
			
			return result;
		}

		static inline Quaternion ForXAxisRotation(float rotationInRadians)
		{
			return ForUnitAxisRotation(Vector3::XAxis(), rotationInRadians);
		}

		static inline Quaternion ForYAxisRotation(float rotationInRadians)
		{
			return ForUnitAxisRotation(Vector3::YAxis(), rotationInRadians);
		}

		static inline Quaternion ForZAxisRotation(float rotationInRadians)
		{
			return ForUnitAxisRotation(Vector3::ZAxis(), rotationInRadians);
		}

		static inline Quaternion Identity()
		{
			return Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
		}

		inline Vector3 GetVectorPart() const
		{
			return Vector3(x, y, z);
		}

		inline Quaternion InverseFromUnit() const
		{
			return Quaternion(w, -x, -y, -z);
		}

		inline Quaternion HamiltonProduct(Quaternion other) const
		{
			Vector3 vector1 = GetVectorPart();
			Vector3 vector2 = other.GetVectorPart();

			float scalarPart = w * other.w - vector1.Dot(vector2);
			Vector3 vectorPart = vector1 * other.w + vector2 * w + vector1.Cross(vector2);

			return Quaternion::FromScalarAndVector(scalarPart, vectorPart);
		}

		inline Quaternion operator*(Quaternion other) const
		{
			return HamiltonProduct(other);
		}

		inline Vector3 operator*(Vector3 original) const
		{
			Vector3 wCrossX = GetVectorPart().Cross(original);
			return original + wCrossX * w * 2.0f + GetVectorPart().Cross(wCrossX) * 2.0f;
		}

		inline void Print() const
		{
			std::cout << "trcr::Quaternion { w: " << w <<
				"; x: " << x <<
				"; y: " << y <<
				"; z: " << z << "; }" << std::endl;
		}
	};
}