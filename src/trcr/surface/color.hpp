#pragma once

#include <cmath>
#include <memory>


namespace trcr {
	struct ColorUInt8
	{
		uint8_t r, g, b, a;

		ColorUInt8() = default;
 
		inline ColorUInt8(uint8_t p_R, uint8_t p_G, uint8_t p_B, uint8_t p_A=255)
			: r(p_R), g(p_G), b(p_B), a(p_A)
		{
		}

		inline static ColorUInt8 Black()
		{
			return ColorUInt8(0, 0, 0);
		}

		inline static ColorUInt8 Blue()
		{
			return ColorUInt8(0, 0, 255);
		}

		inline static ColorUInt8 Green()
		{
			return ColorUInt8(0, 255, 0);
		}

		inline static ColorUInt8 Red()
		{
			return ColorUInt8(255, 0, 0);
		}

		inline static ColorUInt8 White()
		{
			return ColorUInt8(255, 255, 255);
		}
	};

	struct ColorAbsorbed
	{
		float r, g, b;

		inline ColorAbsorbed() = default;

		inline ColorAbsorbed(float redRatio, float greenRatio, float blueRatio)
			: r(redRatio), g(greenRatio), b(blueRatio)
		{
		}

		inline static ColorAbsorbed Black()
		{
			return ColorAbsorbed(1.0f, 1.0f, 1.0f);
		}

		inline static ColorAbsorbed Blue()
		{
			return ColorAbsorbed(1.0f, 1.0f, 0.0f);
		}

		inline static ColorAbsorbed Green()
		{
			return ColorAbsorbed(1.0f, 0.0f, 1.0f);
		}

		inline static ColorAbsorbed Red()
		{
			return ColorAbsorbed(0.0f, 1.0f, 1.0f);
		}

		inline static ColorAbsorbed White()
		{
			return ColorAbsorbed(0.0f, 0.0f, 0.0f);
		}
	};

	struct ColorQuantity
	{
		float r, g, b;

		inline ColorQuantity() = default;

		inline ColorQuantity(float p_R, float p_G, float p_B)
			: r(p_R), g(p_G), b(p_B)
		{
		}

		inline static ColorQuantity None()
		{
			return ColorQuantity(0.0f, 0.0f, 0.0f);
		}

		inline ColorUInt8 ToUInt8(float minimumBrightness, float maximumBrightness) const
		{
			float deltaBrightness = maximumBrightness - minimumBrightness;

			ColorQuantity colorInRange(
				std::round(255.0f * (r - minimumBrightness) / deltaBrightness),
				std::round(255.0f * (g - minimumBrightness) / deltaBrightness),
				std::round(255.0f * (b - minimumBrightness) / deltaBrightness)
			);

			colorInRange.Clamp(0.0f, 255.0f);

			return ColorUInt8(
				(uint8_t) colorInRange.r,
				(uint8_t) colorInRange.g,
				(uint8_t) colorInRange.b
			);
		}

		inline ColorQuantity Clamped(float minimum, float maximum) const
		{
			return ColorQuantity(
				std::max(minimum, std::min(maximum, r)),
				std::max(minimum, std::min(maximum, g)),
				std::max(minimum, std::min(maximum, b))
			);
		}

		inline void Clamp(float minimum, float maximum)
		{
			r = std::max(minimum, std::min(maximum, r));
			g = std::max(minimum, std::min(maximum, g));
			b = std::max(minimum, std::min(maximum, b));
		}

		inline void Absorb(ColorAbsorbed toAbsorb)
		{
			r *= (1 - toAbsorb.r);
			g *= (1 - toAbsorb.g);
			b *= (1 - toAbsorb.b);
		}

		inline ColorQuantity operator+(ColorQuantity other) const
		{
			return ColorQuantity(r + other.r, g + other.g, b + other.b);
		}

		inline ColorQuantity operator-(ColorQuantity other) const
		{
			return ColorQuantity(r - other.r, g - other.g, b - other.b);
		}

		inline ColorQuantity operator*(float scalar) const
		{
			return ColorQuantity(r * scalar, g * scalar, b * scalar);
		}

		inline void operator+=(ColorQuantity other)
		{
			r += other.r;
			g += other.g;
			b += other.b;
		}

		inline void operator-=(ColorQuantity other)
		{
			r -= other.r;
			g -= other.g;
			b -= other.b;
		}

		inline void operator*=(float scalar)
		{
			r *= scalar;
			g *= scalar;
			b *= scalar;
		}
	};
}