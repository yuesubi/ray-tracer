#pragma once

#include "raylib-cpp/Image.hpp"
#include "raylib-cpp/Texture.hpp"

#include "surface.hpp"


namespace trcr {
	/**
	 * @brief An implementation of the surf class for use with raylib.
	 */
	class RaylibSurface : public Surface
	{
	public:
		RaylibSurface(std::size_t width, std::size_t height);

		virtual ColorUInt8 GetPixel(std::size_t x, std::size_t y) const override;
		virtual void SetPixel(std::size_t x, std::size_t y, ColorUInt8 color) override;

		virtual std::size_t Width() const override;
		virtual std::size_t Height() const override;

		const raylib::Image& GetImage() const;
		raylib::Texture GetTexture() const;

	private:
		raylib::Image m_Image;
	};
}