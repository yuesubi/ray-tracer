#include "raylib_surface.hpp"


namespace trcr {
	RaylibSurface::RaylibSurface(std::size_t width, std::size_t height)
		: m_Image(GenImageColor((int) width, (int) height, BLACK))
	{
	}

	ColorUInt8 RaylibSurface::GetPixel(std::size_t x, std::size_t y) const
	{
		Color color = GetImageColor(m_Image, (int) x, (int) y);
		return ColorUInt8(color.r, color.g, color.b, color.a);
	}

	void RaylibSurface::SetPixel(std::size_t x, std::size_t y, ColorUInt8 color)
	{
		m_Image.DrawPixel((int) x, (int) y, { color.r, color.g, color.b, color.a });
	}

	std::size_t RaylibSurface::Width() const
	{
		return m_Image.GetWidth();
	}

	std::size_t RaylibSurface::Height() const
	{
		return m_Image.GetHeight();
	}

	const raylib::Image& RaylibSurface::GetImage() const
	{
		return m_Image;
	}

	raylib::Texture RaylibSurface::GetTexture() const
	{
		raylib::Texture2D texture;
		texture.Load(m_Image);
		return texture;
	}

}