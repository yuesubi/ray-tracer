#pragma once

#include <vector>

#include "surface.hpp"


namespace trcr {
	/**
	 * @brief An implementation of the surf class for use without external
	 * librairies.
	 */
	class BasicSurface : public Surface
	{
	public:
		BasicSurface(std::size_t width, std::size_t height);

		virtual ColorUInt8 GetPixel(std::size_t x, std::size_t y) const override;
		virtual void SetPixel(std::size_t x, std::size_t y, ColorUInt8 color) override;

		virtual std::size_t Width() const override;
		virtual std::size_t Height() const override;

	private:
		std::size_t m_Width, m_Height;
		std::vector<ColorUInt8> m_Buff;
	};
}