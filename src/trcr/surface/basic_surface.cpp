#include "basic_surface.hpp"


namespace trcr {
	BasicSurface::BasicSurface(std::size_t width, std::size_t height)
		: m_Width(width), m_Height(height), m_Buff()
	{
		m_Buff.resize(width * height);
	}

	ColorUInt8 BasicSurface::GetPixel(std::size_t x, std::size_t y) const
	{
		return m_Buff[x + y * m_Width];
	}

	void BasicSurface::SetPixel(std::size_t x, std::size_t y, ColorUInt8 color)
	{
		m_Buff[x + y * m_Width] = color;
	}

	std::size_t BasicSurface::Width() const
	{
		return m_Width;
	}

	std::size_t BasicSurface::Height() const
	{
		return m_Height;
	}
}