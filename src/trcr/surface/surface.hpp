#pragma once

#include <memory>

#include "color.hpp"


namespace trcr {
	/**
	 * @brief A class that represents an image. Inherit from this class to
	 * use it.
	 */
	class Surface
	{
	public:
		virtual ColorUInt8 GetPixel(std::size_t x, std::size_t y) const = 0;
		virtual void SetPixel(std::size_t x, std::size_t y, ColorUInt8 color) = 0;
		virtual std::size_t Width() const = 0;
		virtual std::size_t Height() const = 0;
	};
}