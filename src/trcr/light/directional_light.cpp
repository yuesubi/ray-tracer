#include "directional_light.hpp"


namespace trcr {
	DirectionalLight::DirectionalLight(Vector3 direction, ColorQuantity colorEmitted)
		: m_Direction(direction.Unit()), m_ColorEmited(colorEmitted)
	{
	}

	ColorQuantity DirectionalLight::ColorEmittedTo(Vector3 _position) const
	{
		return m_ColorEmited;
	}

	Vector3 DirectionalLight::LightUnitDirectionTo(Vector3 _position) const
	{
		return m_Direction;
	}

	Ray DirectionalLight::GetShadowRayFrom(Vector3 position) const
	{
		return Ray(position, m_Direction * -1.0f);
	}
}