#pragma once

#include "../camera/ray.hpp"
#include "../surface/color.hpp"


namespace trcr {
	class Light
	{
	public:
		virtual ColorQuantity ColorEmittedTo(Vector3 position) const = 0;
		virtual Vector3 LightUnitDirectionTo(Vector3 position) const = 0;
		virtual Ray GetShadowRayFrom(Vector3 position) const = 0;
	};
}