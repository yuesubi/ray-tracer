#pragma once

#include "light.hpp"


namespace trcr {
	class DirectionalLight : public Light
	{
	public:
		DirectionalLight(Vector3 direction, ColorQuantity colorEmitted);

		virtual ColorQuantity ColorEmittedTo(Vector3 position) const override;
		virtual Vector3 LightUnitDirectionTo(Vector3 position) const override;
		virtual Ray GetShadowRayFrom(Vector3 position) const override;

	private:
		Vector3 m_Direction;
		ColorQuantity m_ColorEmited;
	};
}