# <><><>< VARIABLES TO CHANGE ><><><>

TARGET_EXEC := program.exe

BUILD_DIR  := ./bin
LIB_DIRS   := ./lib
SRC_DIRS   := ./src ./inc

CXX := g++
CXXFLAGS := -Wall -O2

WINDOWS_LINK_FLAGS := -lraylib -lopengl32 -lgdi32 -lwinmm -lraylib
LINUX_LINK_FLAGS := -lrayliblinux -lGL -lm -lpthread -ldl -lrt -lX11

# You need to give .cpp files yourself on windows.
# In linux it automaticly detects them
SRC_FILES := ./src/main.cpp ./src/trcr/surface/raylib_surface.cpp ./src/trcr/surface/basic_surface.cpp ./src/trcr/object/plane.cpp ./src/trcr/object/sphere.cpp ./src/trcr/object/triangle.cpp ./src/trcr/camera/camera.cpp ./src/trcr/renderer/renderer.cpp ./src/trcr/renderer/color_coefficients.cpp ./src/trcr/light/directional_light.cpp


# <><><>< COMMANDS AND INSTRUCTIONS ><><><>

CURRENT_PLATFORM := none
ifeq ($(OS),Windows_NT)
	CURRENT_PLATFORM := windows
else
	UNAMEOS = $(shell uname)
	ifeq ($(UNAMEOS),Linux)
		CURRENT_PLATFORM := linux
	endif
endif


LINK_FLAGS :=
SRCS :=
INC_DIRS := 

ifeq ($(CURRENT_PLATFORM),windows)
	# Use windows's link flags
	LINK_FLAGS += $(WINDOWS_LINK_FLAGS)

	# Set C++ files we want to compile
	SRCS := $(SRC_FILES)

	# Folders with header files
	INC_DIRS := $(SRC_DIRS)

else ifeq ($(CURRENT_PLATFORM),linux)
	# Use linux's link flags
	LINK_FLAGS += $(LINUX_LINK_FLAGS)

	# Find all the C++ files we want to compile
	SRCS := $(shell find $(SRC_DIRS) -name '*.cpp')

	# Every folder needs to be passed to G++ so that it can find header files
	INC_DIRS := $(shell find $(SRC_DIRS) -type d)

endif


# String substitution for every C++ file.
# As an example, hello.cpp turns into ./build/hello.cpp.o
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

INC_FLAGS := $(addprefix -I,$(INC_DIRS))
LIB_FLAGS := $(addprefix -L,$(LIB_DIRS))

CPPFLAGS := $(INC_FLAGS)
LDFLAGS := $(LIB_FLAGS) $(LINK_FLAGS)


default: run


run: $(BUILD_DIR)/$(TARGET_EXEC)
	@echo "<><><>< RUNNING $< ><><><>"
	@exec $<


# The final build step.
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	@echo "<><><>< LINKING ><><><>"
	"$(CXX)" $(OBJS) -o $@ $(LDFLAGS)
	@echo ""

# Build step for C++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	@echo "<><><>< COMPILING $< TO $@ ><><><>"
	@mkdir -p $(dir $@)
	"$(CXX)" $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
	@echo ""


setup:
	@echo "<><><>< MAKE DIRECTORIES ><><><>"
	mkdir -p $(LIB_DIRS)
	mkdir -p $(SRC_DIRS)


.PHONY: clean
clean:
	@echo "<><><>< CLEANING $(BUILD_DIR) ><><><>"
	@rm -r $(BUILD_DIR)
